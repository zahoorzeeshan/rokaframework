﻿using Roka.Backbone.Domain;
using Roka.Backbone.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roka.Backbone.DTO.User
{
    public class LoginResultDTO
    {
        public LoginResult LoginResult { get; set; }
        public RokaUser User { get; set; }
    }
}

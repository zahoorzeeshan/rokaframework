﻿using Roka.Backbone.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roka.Backbone.Security
{
    public interface IUserContext
    {
        RokaUser User { get; }
    }
}

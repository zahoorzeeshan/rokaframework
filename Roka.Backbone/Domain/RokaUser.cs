﻿using Microsoft.AspNet.Identity;
using Roka.Backbone.Audit.Types;
using Roka.Backbone.Data.Entity;
using Roka.Backbone.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roka.Backbone.Domain
{
    public class RokaUser : BaseEntity, IEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public int WrongPasswordCount { get; set; }
        public DateTime ActiveFrom { get; set; }
        public DateTime? ActiveTo { get; set; }
        public string Status { get; set; }
        public RokaUserAccountStatus RokaUserAccountStatus { get; set; }
    }
}

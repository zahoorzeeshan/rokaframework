﻿using Roka.Backbone.DTO;
using Roka.Backbone.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roka.Backbone.Services
{
    public interface IAccountService
    {
        LoginResultDTO AuthenticateUser(string UserName, string Password);
    }
}

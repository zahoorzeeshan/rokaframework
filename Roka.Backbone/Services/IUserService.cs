﻿using Roka.Backbone.Domain;
using Roka.Backbone.DTO;
using Roka.Backbone.Pagination;
using System.Collections.Generic;

namespace Roka.Backbone.Services
{
    public interface IUserService
    {
        IList<UserListDTO> GetUsersList();
        UserListDTO GetUserDTOById(int Id);
        RokaUser GetUserById(int Id);
        PagedResult<UserListDTO> Filter(PaginationArgs args, UserListDTOSpec spec);
    }
}

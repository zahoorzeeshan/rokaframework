﻿using Autofac;
using Roka.Backbone.Exception;
using Roka.Core.Exception.Internals;

namespace Roka.Core.Exception
{
    internal class StartupTasks : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ExceptionHandler>().As<IExceptionHandler>().InstancePerLifetimeScope();
            base.Load(builder);
        }
    }
}

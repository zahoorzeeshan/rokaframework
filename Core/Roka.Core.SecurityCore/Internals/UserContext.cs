﻿using System;
using Roka.Backbone.Audit;
using Roka.Backbone.Domain;
using Roka.Backbone.Security;

namespace Roka.Core.SecurityCore.Internals
{
    internal class UserContext : IUserContext
    {
        public UserContext()
        {
        }

        public User User { get; set; }
    }
}

﻿using Roka.Backbone.Domain;
using Roka.Backbone.Security;
using System.Threading;

namespace Roka.Core.SecurityCore.Internals
{
    internal class WebUserContextFactory
    {
        public static IUserContext InitiateUserContext()
        {
            UserContext UserContext = new UserContext();
            UserContext.User = new User();
            return UserContext;
        }
    }
}

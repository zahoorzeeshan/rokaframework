﻿using Autofac;
using Roka.Backbone.Security;
using Roka.Core.SecurityCore.Internals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roka.SecurityCore.Security
{
    public class StartupTasks : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.Register(c =>
            {
                c.Resolve< IHttpContextAccessor >
                return WebUserContextFactory.InitiateUserContext();
            }).As<IUserContext>().InstancePerLifetimeScope();
            base.Load(builder);
        }
    }
}

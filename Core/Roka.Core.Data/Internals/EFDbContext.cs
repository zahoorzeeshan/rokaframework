﻿using Microsoft.EntityFrameworkCore;
using Roka.Backbone.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Roka.Core.Data.Internals
{
    internal class EFDbContext : DbContext
    {
        public EFDbContext()
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string AssemblyPath = "Roka.Backbone";
            Assembly Assembly = AppDomain.CurrentDomain.GetAssemblies()
                .FirstOrDefault(assembly => assembly.GetName().Name == AssemblyPath);

            MethodInfo EntityMethod = typeof(ModelBuilder).GetMethods()
                .Where(m => m.Name.Equals("Entity"))
                .Where(m => m.GetParameters().Count() == 0)
                .First();

            Assembly.GetTypes()
                .Where(m => m.BaseType != null)
                .Where(m => m.BaseType.FullName == typeof(BaseEntity).FullName)
                .ToList()
                .ForEach(m => EntityMethod.MakeGenericMethod(m).Invoke(modelBuilder, new object[] { }));
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IDbConnection connection = new SqlConnection("Data Source=artifexpay.database.windows.net;Initial Catalog=artifexpaydb;User ID=artifexuser;Password=Pay135790;Integrated Security=False;");
            optionsBuilder.UseSqlServer((DbConnection)connection);
        }
    }
}

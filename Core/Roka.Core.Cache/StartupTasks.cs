﻿using Autofac;
using Roka.Backbone.Cache;
using Roka.Core.Cache.Internals;

namespace Roka.Core.Cache
{
    internal class StartupTasks : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RuntimeCacheStorage>().As<ICacheStorage>().InstancePerLifetimeScope();
            base.Load(builder);
        }
    }
}

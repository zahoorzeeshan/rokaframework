﻿using Autofac;
using Microsoft.AspNetCore.Http;
using Roka.Backbone.Security;
using Roka.Backbone.Services;
using Roka.Core.Security.Internals;

namespace Roka.Core.Security
{
    internal class StartupTasks : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DefaultAuthenticationProvider>().As<IAuthenticationProvider>().InstancePerLifetimeScope();

            builder.Register(ComponentContext =>
            {
                IHttpContextAccessor httpContextAccessor = ComponentContext.Resolve<IHttpContextAccessor>();
                //IUserService userService = ComponentContext.Resolve<IUserService>();

                return WebUserContextFactory.InitiateUserContext();

            }).As<IUserContext>().InstancePerLifetimeScope();
            base.Load(builder);
        }
    }
}

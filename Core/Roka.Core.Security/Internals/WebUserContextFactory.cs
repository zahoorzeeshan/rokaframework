﻿using Roka.Backbone.Domain;
using Roka.Backbone.Security;
using System.Threading;

namespace Roka.Core.Security.Internals
{
    internal class WebUserContextFactory
    {
        public static IUserContext InitiateUserContext()
        {
            UserContext UserContext = new UserContext();
            UserContext.User = new RokaUser();
            return UserContext;
        }
    }
}

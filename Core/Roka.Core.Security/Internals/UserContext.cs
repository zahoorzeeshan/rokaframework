﻿using System;
using Roka.Backbone.Audit;
using Roka.Backbone.Domain;
using Roka.Backbone.Security;

namespace Roka.Core.Security.Internals
{
    internal class UserContext : IUserContext
    {
        public UserContext()
        {
        }

        public RokaUser User { get; set; }
    }
}

﻿import React, { Component } from "react";
import { RokaGrid } from "../../Components/RokaGrid";
import  withRoka  from '../../Components/Roka/RokaBaseComponent';

const Columns = [
    { name: 'id', title: 'ID' },
    { name: 'name', title: 'Name' },
    { name: 'surname', title: 'SurName' },
];

class GridTest extends React.Component {
    render() {
        console.log(this.props);
        return (
            <RokaGrid
                Columns={Columns}
                PageSize={5}
                DataPath="/SampleData/FillUserGrid/"
            ></RokaGrid>
        );
    }
}
GridTest = withRoka(GridTest);
export { GridTest };

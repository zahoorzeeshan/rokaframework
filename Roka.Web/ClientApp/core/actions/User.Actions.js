﻿import { UserConstants } from '../constants';

export const UserActions = {
    login,
};

function login(username, password) {
    return dispatch => {
        dispatch(request({ username }));
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ username, password })
        };

        return fetch('http://localhost:60672/Authenticate/login', requestOptions)
            .then(handleResponse, handleError)
            .then(result => {
                if (result.success) {
                    //localStorage.setItem('user', JSON.stringify(user));
                    dispatch(success(result.data.user));
                }
            });

    };

    function request(user) { return { type: UserConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: UserConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: UserConstants.LOGIN_FAILURE, error } }
}

function handleResponse(response) {
    return new Promise((resolve, reject) => {
        if (response.ok) {
            var contentType = response.headers.get("content-type");
            if (contentType && contentType.includes("application/json")) {
                response.json().then(json => resolve(json));
            } else {
                resolve();
            }
        } else {
            response.text().then(text => reject(text));
        }
    });
}

function handleError(error) {
    return Promise.reject(error && error.message);
}



import React, { Component } from "react";
import { Route, Switch } from "react-router-dom"

import { Dashboard }  from "./Modules/Dashboard";
import { LoginPage } from "./Modules/Account";
import { GridTest } from "./Modules/Test/GridTest";

import  PersistentDrawer  from "./Modules/Components/Drawer/PersistentDrawer";

import { Provider } from 'react-redux';
import { store } from './core/store/store';
import { Layout } from "./components/Layout";
import { Login } from "./components/Login";

const AppRoute = ({ component: Component, layout: Layout }) => (
    <Route render={props => (
        <Layout>
            <Component {...props} />
        </Layout>
    )} />
)

const MainLayout = props => (
    <div>
        <h1>Main</h1>
        {props.children}
    </div>
)


export const routes = (
    <Provider store={store}>
        <Switch>
            <AppRoute exact path="/" layout={PersistentDrawer} component={Dashboard} />
            <AppRoute exact path="/login" layout={MainLayout} component={LoginPage} />
            <AppRoute exact path="/grid" layout={PersistentDrawer} component={GridTest} />
        </Switch>
    </Provider>
);

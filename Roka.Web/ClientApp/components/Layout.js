import React, { Component } from "react";
import { NavMenu } from "./NavMenu";
import PersistentDrawer from "./Drawer";
import { Provider } from 'react-redux';
import { store } from '../core/store/store';

export class Layout extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-3">
                            <NavMenu />
                        </div>
                        <div className="col-sm-9">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </Provider>
        );
    }
}

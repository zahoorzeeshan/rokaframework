import React, { Component } from "react";
import { fakeAuth } from '../routes';

export class Login extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div>
                <h1>Weather forecast</h1>
                <p>This component demonstrates fetching data from the server.</p>
                <button onPress={() => { fakeAuth.authenticate(20000); }}>Login</button>
            </div>
        );
    }
}

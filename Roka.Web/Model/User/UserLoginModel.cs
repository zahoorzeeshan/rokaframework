﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roka.Web.Model.User
{
    public class UserLoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}

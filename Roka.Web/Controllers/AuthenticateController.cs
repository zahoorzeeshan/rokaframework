﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Roka.Backbone.DTO.User;
using Roka.Backbone.Enums;
using Roka.Backbone.Services;
using Roka.Web.Model.Base;
using Roka.Web.Model.User;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Roka.Web.Controllers
{
    [Produces("application/json")]
    //[Route("api/Authenticate")]
    public class AuthenticateController : Controller
    {
        protected readonly IAccountService _accountService;
        public AuthenticateController(IAccountService AccountService)
        {
            _accountService = AccountService;
        }
        public IActionResult Login(UserLoginModel model)
        {
            if (ModelState.IsValid)
            {
                LoginResultDTO result = _accountService.AuthenticateUser(model.UserName, model.Password);
                if (result.LoginResult == LoginResult.Success)
                {

                    var tokenHandler = new JwtSecurityTokenHandler();   
                    var key = Encoding.ASCII.GetBytes("alskdfj2398r49qhroqh9238yroinehofiuy3h948rhoiefa");
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                            new Claim(ClaimTypes.Name, result.User.Id.ToString()),
                            new Claim(ClaimTypes.Name, result.User.Username.ToString())
                        }),
                        Expires = DateTime.UtcNow.AddDays(7),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    var tokenString = tokenHandler.WriteToken(token);


                    return Result.Create(true, result);
                }
            }
            return Result.Create(false);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Roka.Backbone.Domain;
using Roka.Backbone.DTO;
using Roka.Backbone.Pagination;
using Roka.Backbone.Services;

namespace Roka.Web.Controllers
{
    public class SampleDataController : Controller
    {
        protected readonly IUserService _userService;
        public SampleDataController(IUserService UserService)
        {
            _userService = UserService;
        }

        [HttpPost]
        public IActionResult FillUserGrid([FromBody]PaginationArgs args, UserListDTOSpec spec)
        {
           var result = _userService.Filter(args, spec);

            

            return Json(result);
        }

    }
}

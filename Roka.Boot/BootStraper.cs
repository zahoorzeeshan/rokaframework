﻿using Autofac;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Roka.Boot
{
    public class Bootstraper
    {
        public static void DoTheMagic(ContainerBuilder builder)
        {
            string LibrariesPath = Path.Combine(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()), "libs");

            Assembly[] CoreAndServiceAssemblies = Directory
                .GetFiles(LibrariesPath, "Roka.*.dll")
                .Select(m => Assembly.LoadFile(m))
                .ToArray();

            builder.RegisterAssemblyModules(CoreAndServiceAssemblies.ToArray());

        }
    }
}
